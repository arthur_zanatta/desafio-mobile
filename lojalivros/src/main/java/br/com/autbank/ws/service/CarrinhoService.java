package br.com.autbank.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.autbank.ws.model.Carrinho;
import br.com.autbank.ws.repository.CarrinhoRepository;

@Service
public class CarrinhoService {
	
	@Autowired
	CarrinhoRepository carrinhoRepository;
	
	public Carrinho cadastrar(Carrinho carrinho) {		
		return carrinhoRepository.save(carrinho);		
	}
	
	public List<Carrinho> buscarTodos() {
		return carrinhoRepository.findAll();
	}
	
	public void excluir(Carrinho carrinho) {
		carrinhoRepository.delete(carrinho);
	}
	
	public Carrinho buscarPorId(Integer id) {
		return carrinhoRepository.findById(id).get();
	}
	
	public Carrinho alterar(Carrinho carrinho) {
		return carrinhoRepository.save(carrinho);
	}

}
