package br.com.autbank.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.autbank.ws.model.Livro;
import br.com.autbank.ws.repository.LivroRepository;

@Service
public class LivroService {
	
	@Autowired
	LivroRepository livroRepository;
	
	public Livro cadastrar(Livro livro) {		
		return livroRepository.save(livro);		
	}
	
	public List<Livro> buscarTodos() {
		return livroRepository.findAll();
	}
	
	public void excluir(Livro livro) {
		livroRepository.delete(livro);
	}
	
	public Livro buscarPorId(Integer id) {
		return livroRepository.findById(id).get();
	}
	
	public Livro alterar(Livro livro) {
		return livroRepository.save(livro);
	}

}
