package br.com.autbank.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.autbank.ws.model.Livro;
import br.com.autbank.ws.service.LivroService;

@RestController
@RequestMapping
public class LivroController {
	
	@Autowired
	LivroService livroService;
	
	@RequestMapping(method=RequestMethod.POST, value="/livros", consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Livro> cadastrarLivro(@RequestBody Livro livro) {
		
		Livro livroCadastrado = livroService.cadastrar(livro);
		return new ResponseEntity<>(livroCadastrado, HttpStatus.CREATED);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/livros", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Livro>> BuscarTodosLivros() {
		
		List<Livro> livrosBuscados = livroService.buscarTodos();
		return new ResponseEntity<>(livrosBuscados, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/livros/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Livro> BuscarLivroPorId(Integer id) {
		
		Livro livro = livroService.buscarPorId(id);
		return new ResponseEntity<>(livro, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/livros/{id}")
	public ResponseEntity<Livro> excluirLivro(@PathVariable Integer id) {
		
		Livro livroEncontrado = livroService.buscarPorId(id);
		
		if(livroEncontrado == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		livroService.excluir(livroEncontrado);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/livros", consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Livro> alterarLivro(@RequestBody Livro livro) {
		
		Livro livroAlterado = livroService.alterar(livro);
		return new ResponseEntity<>(livroAlterado, HttpStatus.OK);
	}

}
