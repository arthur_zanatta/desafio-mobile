package br.com.autbank.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.autbank.ws.model.Carrinho;
import br.com.autbank.ws.service.CarrinhoService;

@RestController
@RequestMapping
public class CarrinhoController {
	
	@Autowired
	CarrinhoService carrinhoService;
	
	@RequestMapping(method=RequestMethod.POST, value="/carrinhos", consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Carrinho> cadastrarCarrinho(@RequestBody Carrinho carrinho) {
		
		Carrinho carrinhoCadastrado = carrinhoService.cadastrar(carrinho);
		return new ResponseEntity<>(carrinhoCadastrado, HttpStatus.CREATED);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/carrinhos", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Carrinho>> BuscarTodosCarrinhos() {
		
		List<Carrinho> carrinhosBuscados = carrinhoService.buscarTodos();
		return new ResponseEntity<>(carrinhosBuscados, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/carrinhos/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Carrinho> BuscarCarrinhoPorId(Integer id) {
		
		Carrinho carrinho = carrinhoService.buscarPorId(id);
		return new ResponseEntity<>(carrinho, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/carrinhos/{id}")
	public ResponseEntity<Carrinho> excluirCarrinho(@PathVariable Integer id) {
		
		Carrinho carrinhoEncontrado = carrinhoService.buscarPorId(id);
		
		if(carrinhoEncontrado == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		carrinhoService.excluir(carrinhoEncontrado);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/carrinhos", consumes=MediaType.APPLICATION_JSON_VALUE,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Carrinho> alterarCarrinho(@RequestBody Carrinho carrinho) {
		
		Carrinho carrinhoAlterado = carrinhoService.alterar(carrinho);
		return new ResponseEntity<>(carrinhoAlterado, HttpStatus.OK);
	}
}
