var appLivro = angular.module("appLivro", ['ngRoute']);

appLivro.config(function($routeProvider, $locationProvider) {
	$routeProvider
	.when("/livros", {templateUrl:'view/livro.html', controller:'livroController'})
	.when("/carrinhos", {templateUrl:'view/carrinho.html', controller:'carrinhoController'})
	.otherwise({rediretTo:'/'});
	
	$locationProvider.html5Mode(true);
});