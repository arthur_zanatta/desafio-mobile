appLivro.controller("carrinhoController", function($scope, $http) {
	//$scope.carrinhos = [];
	$scope.carrinho = {};
	$scope.livros = [];
	$scope.livro = {};
	
	carregarCarrinhos = function() {
		$http({method:'GET', url:'/carrinhos'}).then(function(response) {
			$scope.carrinhos = response.data;
		}, function(response) {
			console.log(response.data);
			console.log(response.status);
		});
	}
	
	$scope.salvarCarrinho = function() {
		if($scope.frmCarrinho.$valid) {
			$http({method:'POST', url:'/carrinhos', data:$scope.carrinho}).then(function(response) {
				//carregarCarrinhos();
				$scope.cancelarAlteracaoCarrinho();
				$scope.frmCarrinho.$setPristine(true);
			}, function(response) {
				console.log(response.data);
				console.log(response.status);
			});
		} else {
			window.alert("Dados inválidos");
		}
	}
	
	$scope.excluirCarrinho = function(carrinho) {
		$http({method:'DELETE', url:'/carrinhos/' + carrinho.id, data:$scope.carrinho}).then(function(response) {
			pos = $scope.carrinhos.indexOf(carrinho);
			$scope.carrinhos.splice(pos, 1);
		}, function(response) {
			console.log(response.data);
			console.log(response.status);
		});
	}
	
	$scope.alterarCarrinho = function(car) {
		$scope.carrinho = angular.copy(car);
	};
	
	$scope.cancelarAlteracaoCarrinho = function() {
		$scope.carrinho = {};
	}
	
	$scope.adicionarLivro = function() {
		$http({method:'POST', url:'/livros', data:$scope.livro}).then(function(response) {
		    carregarLivros();
			$scope.cancelarAlteracaoLivro();
		}, function(response) {
			console.log(response.data);
			console.log(response.status);
		});
	}
	
	carregarLivros = function() {
		$http({method:'GET', url:'/livros'}).then(function(response) {
			$scope.livros = response.data;
		}, function(response) {
			console.log(response.data);
			console.log(response.status);
		});
	}
	
	/*$scope.calcularCompra = function() {
		tam = $scope.carrinho.listaLivros.size();
		for(li : $scope.carrinho.listaLivros) {
			$scope.carrinho.total += li.preco;
		}
		if(tam == 2) $scope.carrinho.total *= 0.95;
		else if(tam == 3) $scope.carrinho.total *= 0.9;
		else if(tam == 4) $scope.carrinho.total *= 0.8;
		if(tam >= 5) $scope.carrinho.total *= 0.75;
	}*/
	
	//carregarCarrinhos();
	carregarLivros();
});