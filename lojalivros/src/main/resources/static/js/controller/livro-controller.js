appLivro.controller("livroController", function($scope, $http) {
	$scope.livros = [];
	$scope.livro = {};
	
	carregarLivros = function() {
		$http({method:'GET', url:'/livros'}).then(function(response) {
			$scope.livros = response.data;
		}, function(response) {
			console.log(response.data);
			console.log(response.status);
		});
	};
	
	$scope.salvarLivro = function() {
		if($scope.frmLivro.$valid) {
			$http({method:'POST', url:'/livros', data:$scope.livro}).then(function(response) {
				carregarLivros();
				$scope.cancelarAlteracaoLivro();
				$scope.frmLivro.$setPristine(true);
			}, function(response) {
				console.log(response.data);
				console.log(response.status);
			});
		}
		else {
			window.alert("Dados inválidos");
		}
	};
	
	$scope.excluirLivro = function(livro) {
		$http({method:'DELETE', url:'/livros/' + livro.id, data:$scope.livro}).then(function(response) {
			pos = $scope.livros.indexOf(livro);
			$scope.livros.splice(pos, 1);
		}, function(response) {
			console.log(response.data);
			console.log(response.status);
		});
	};
	
	$scope.alterarLivro = function(li) {
		$scope.livro = angular.copy(li);
	};
	
	$scope.cancelarAlteracaoLivro = function() {
		$scope.livro = {};
	};
	
	carregarLivros();
});